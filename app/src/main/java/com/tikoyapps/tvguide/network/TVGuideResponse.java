package com.tikoyapps.tvguide.network;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android 17 on 6/5/2015.
 */
public class TVGuideResponse {
    private int count;
    private List<TVShow> results = new ArrayList<TVShow>();

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<TVShow> getResults() {
        return results;
    }

    public void setResults(List<TVShow> results) {
        this.results = results;
    }

}
