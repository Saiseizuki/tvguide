package com.tikoyapps.tvguide.network;

import com.tikoyapps.tvguide.BuildConfig;

import retrofit.RestAdapter;

/**
 * Created by Android 17 on 6/5/2015.
 */
public class RestHolder {

    private static Api api;

    public static Api getRestAdapter(){
        if (api == null){
            synchronized (RestHolder.class) {
                if (api == null) {
                    api = new RestAdapter.Builder()
                            .setLogLevel(RestAdapter.LogLevel.FULL)
                            .setEndpoint(BuildConfig.SERVER_URL).build().create(Api.class);
                }
            }
        }
        return api;
    }
}
