package com.tikoyapps.tvguide.network;


        import retrofit.Callback;
        import retrofit.http.GET;
        import retrofit.http.Query;

/**
 * Created by Android 17 on 6/5/2015.
 */
public interface Api {
    @GET("/wabz/guide.php")
    void getShows(@Query("start") int start, Callback<TVGuideResponse> tvGuideResponseCallback);
}
