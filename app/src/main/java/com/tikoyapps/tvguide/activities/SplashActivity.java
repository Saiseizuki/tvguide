package com.tikoyapps.tvguide.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.tikoyapps.tvguide.R;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Android 17 on 6/5/2015.
 */
public class SplashActivity extends Activity {


    @InjectView(R.id.tvguide_splashactivity_parentlayout)
    View parentLayout;

    Timer timer;
    boolean isLaunching = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tvguide_splashactivity);
        ButterKnife.inject(this);
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                startLoginActivity();
            }
        }, 3000);
    }

    @OnClick(R.id.tvguide_splashactivity_parentlayout)
    void onClickSplashScreen() {
        startLoginActivity();
    }

    private void startLoginActivity() {
        if (!isLaunching) {
            Intent intent = new Intent(this, ListActivity.class);
            startActivity(intent);
            finish();
            isLaunching = true;
        }
    }
}
