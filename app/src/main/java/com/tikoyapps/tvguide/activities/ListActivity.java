package com.tikoyapps.tvguide.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.tikoyapps.tvguide.R;
import com.tikoyapps.tvguide.TVGuideApplication;
import com.tikoyapps.tvguide.adapters.TVShowAdapter;
import com.tikoyapps.tvguide.network.RestHolder;
import com.tikoyapps.tvguide.network.TVGuideResponse;
import com.tikoyapps.tvguide.network.TVShow;
import com.tikoyapps.tvguide.network.TVShowRealm;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Android 17 on 6/5/2015.
 */
public class ListActivity extends Activity {

    private static final String sharedPrefsName = "TVGUideSharedPrefs";

    private static final String LAST_PAGE_FETCHED = "last_page_fetched";

    @InjectView(R.id.tvguide_listactivity_listview)
    ListView listView;

    TVShowAdapter tvShowAdapter;

    private int preLast = 0;

    Realm realm;

    Activity mActivity;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    private int getLastPageFetched() {
        return sharedPreferences.getInt(LAST_PAGE_FETCHED, 0);
    }

    private void setLastPageFetched(int value) {
        Log.d("**** TVGUIDE ****", value + "");
        editor.putInt(LAST_PAGE_FETCHED, value);
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getInstance(this);
        mActivity = this;

        sharedPreferences = TVGuideApplication.getTVGuideApplicationContext().getSharedPreferences(sharedPrefsName, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        setContentView(R.layout.tvguide_listactivity);
        ButterKnife.inject(this);


        tvShowAdapter = new TVShowAdapter(this, new ArrayList<TVShow>());
        listView.setAdapter(tvShowAdapter);


        callApiAndUpdateList(getLastPageFetched());

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    if (preLast != lastItem) {
                        setLastPageFetched(getLastPageFetched() + 10);
                        callApiAndUpdateList(getLastPageFetched());
                        preLast = lastItem;
                    }
                }
            }
        });

    }

    private void callApiAndUpdateList(final int start) {
        showLoadingLayout();
        RestHolder.getRestAdapter().getShows(start, new Callback<TVGuideResponse>() {
            @Override
            public void success(TVGuideResponse tvGuideResponse, Response response) {

                if (tvGuideResponse.getResults().size() == 0) {
                    Toast.makeText(mActivity, "Last Page Reached", Toast.LENGTH_LONG).show();
                }

                realm.beginTransaction();

                for (TVShow results : tvGuideResponse.getResults()) {
                    TVShowRealm tvShow = realm.createObject(TVShowRealm.class); // Create a new object
                    tvShow.setName(results.getName());
                    tvShow.setStart_time(results.getStart_time());
                    tvShow.setEnd_time(results.getEnd_time());
                    tvShow.setChannel(results.getChannel());
                    tvShow.setRating(results.getRating());
                }

                realm.commitTransaction();


                RealmQuery<TVShowRealm> query = realm.where(TVShowRealm.class);
                RealmResults<TVShowRealm> realmResults = query.findAll();

                ArrayList<TVShow> tvShowArrayList = new ArrayList<TVShow>();

                for (TVShowRealm tvShowRealm : realmResults) {
                    TVShow tvShow = new TVShow();
                    tvShow.setName(tvShowRealm.getName());
                    tvShow.setStart_time(tvShowRealm.getStart_time());
                    tvShow.setEnd_time(tvShowRealm.getEnd_time());
                    tvShow.setChannel(tvShowRealm.getChannel());
                    tvShow.setRating(tvShowRealm.getRating());
                    tvShowArrayList.add(tvShow);
                }

                tvShowAdapter.updateBackingList(tvShowArrayList);
                hideLoadingLayout();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(mActivity, "No connection", Toast.LENGTH_LONG).show();
                RealmQuery<TVShowRealm> query = realm.where(TVShowRealm.class);
                RealmResults<TVShowRealm> realmResults = query.findAll();

                ArrayList<TVShow> tvShowArrayList = new ArrayList<TVShow>();

                for (TVShowRealm tvShowRealm : realmResults) {
                    TVShow tvShow = new TVShow();
                    tvShow.setName(tvShowRealm.getName());
                    tvShow.setStart_time(tvShowRealm.getStart_time());
                    tvShow.setEnd_time(tvShowRealm.getEnd_time());
                    tvShow.setChannel(tvShowRealm.getChannel());
                    tvShow.setRating(tvShowRealm.getRating());
                    tvShowArrayList.add(tvShow);
                }

                tvShowAdapter.updateBackingList(tvShowArrayList);
                hideLoadingLayout();
            }
        });
    }


    private void showLoadingLayout() {
        FrameLayout loadingLayout = (FrameLayout) findViewById(R.id.tvguide_listactivity_loadinglayout);
        loadingLayout.setVisibility(View.VISIBLE);
    }

    private void hideLoadingLayout() {
        FrameLayout loadingLayout = (FrameLayout) findViewById(R.id.tvguide_listactivity_loadinglayout);
        loadingLayout.setVisibility(View.GONE);
    }
}
