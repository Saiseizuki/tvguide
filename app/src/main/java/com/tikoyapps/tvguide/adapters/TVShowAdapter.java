package com.tikoyapps.tvguide.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tikoyapps.tvguide.R;
import com.tikoyapps.tvguide.network.TVShow;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Android 17 on 6/5/2015.
 */
public class TVShowAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<TVShow> tvGuideResponseList;

    public TVShowAdapter(Context context, ArrayList<TVShow> tvGuideResponseArrayList) {
        this.mContext = context;
        this.tvGuideResponseList = tvGuideResponseArrayList;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    public void updateBackingList(ArrayList<TVShow> tvGuideResponseArrayList){
        this.tvGuideResponseList = tvGuideResponseArrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return tvGuideResponseList.size();
    }

    @Override
    public TVShow getItem(int position) {
        return tvGuideResponseList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TVGuideResponseViewHolder viewHolder;

        if (convertView != null) {
            viewHolder = (TVGuideResponseViewHolder) convertView.getTag();
        } else {
            convertView = mLayoutInflater.inflate(R.layout.tvguide_listactivity_listitem, parent, false);
            viewHolder = new TVGuideResponseViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        TVShow tvGuideResponse = getItem(position);
        viewHolder.nameTextView.setText(tvGuideResponse.getName());
        viewHolder.startTimeTextView.setText(tvGuideResponse.getStart_time());
        viewHolder.endTimeTextView.setText(tvGuideResponse.getEnd_time());
        viewHolder.channelTextView.setText(tvGuideResponse.getChannel());
        viewHolder.ratingTextView.setText(tvGuideResponse.getRating());

        return convertView;
    }


    class TVGuideResponseViewHolder {
        @InjectView(R.id.tvguide_listactivity_listitem_name_textview)
        TextView nameTextView;
        @InjectView(R.id.tvguide_listactivity_listitem_starttime_textview)
        TextView startTimeTextView;
        @InjectView(R.id.tvguide_listactivity_listitem_endtime_textview)
        TextView endTimeTextView;
        @InjectView(R.id.tvguide_listactivity_listitem_channel_textview)
        TextView channelTextView;
        @InjectView(R.id.tvguide_listactivity_listitem_rating_textview)
        TextView ratingTextView;

        public TVGuideResponseViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
