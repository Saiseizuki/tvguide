package com.tikoyapps.tvguide;

import android.app.Application;
import android.content.Context;

/**
 * Created by Android 17 on 6/5/2015.
 */
public class TVGuideApplication extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        mContext = this;
        super.onCreate();
    }

    public static Context getTVGuideApplicationContext(){
        return mContext;
    }
}
